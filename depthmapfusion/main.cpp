#include <Eigen/Core>
#include <opencv2/core.hpp>
#include <iostream>
#include <pcl/point_types.h>
#include <pcl/filters/passthrough.h>
#include "pch.h"


#include <iostream>
#include <fstream>
#include <math.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <Eigen/Geometry> 
#include <boost/format.hpp>  // for formating strings
#include <pcl/point_types.h> 
#include <pcl/io/pcd_io.h> 
#include <pcl/visualization/pcl_visualizer.h>

struct posestamped
{
	int64_t timestamp;
	std::string imagename;
	Eigen::Matrix4d poseMatrix;
};

std::vector<posestamped> load_csv(const std::string & path) {
	std::ifstream indata;
	indata.open(path);
	std::string line, header;
	std::vector<posestamped> poses;
	std::getline(indata, header);
	while (std::getline(indata, line)) {
		if (line.length() < 5) { continue; }
		std::stringstream lineStream(line);
		std::string cell;
		posestamped pose_stamped;
		std::vector<double> values;
		std::getline(lineStream, cell, ',');
		pose_stamped.timestamp = std::stoll(cell);
		std::getline(lineStream, cell, ',');
		pose_stamped.imagename = cell;

		for (int i = 0; i < 16; ++i) {
			std::getline(lineStream, cell, ',');
			values.push_back(std::stod(cell));
		}
		Eigen::Matrix4d origin_T_frame = Eigen::Map<
			const Eigen::Matrix<double, 4, 4, Eigen::RowMajor>
		>(values.data(), 4, 4).transpose();
		values.clear();
		for (int i = 0; i < 16; ++i) {
			std::getline(lineStream, cell, ',');
			values.push_back(std::stod(cell));
		}
		Eigen::Matrix4d camera_T_frame = Eigen::Map<
			const Eigen::Matrix<double, 4, 4, Eigen::RowMajor>
		>(values.data(), 4, 4).transpose();
		Eigen::Matrix4d image_T_camera;
		image_T_camera << 1, 0, 0, 0, 0, -1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 1;
		pose_stamped.poseMatrix = image_T_camera * camera_T_frame * origin_T_frame.inverse();
		poses.push_back(pose_stamped);
		std::cout << pose_stamped.poseMatrix << std::endl;

	}
	return poses;
}

int main(int argc, char** argv)
{

	const int depimg_width{ 448 }, depimg_height{ 450 };
	float* ff = new float[depimg_height * depimg_width * 2];
	std::string camera_intrins_file = "/home/youkely/3d_vision/recording_45/long_throw_depth_camera_space_projection.bin";
	std::ifstream ifs(camera_intrins_file, std::ios::binary | std::ios::in);
	ifs.read((char*)ff, sizeof(float) * depimg_height * depimg_width * 2);
	ifs.close();
	std::cout << ff[55555] << std::endl;;
	float proj_u[depimg_height][depimg_width];
	float proj_v[depimg_height][depimg_width];
	for (int i = 0; i < depimg_height; ++i)
	{
		for (int j = 0; j < depimg_width; ++j) {
			proj_u[i][j] = ff[(i + j * depimg_height) * 2];
			proj_v[i][j] = ff[(i + j * depimg_height) * 2 + 1];
		}

		std::vector<posestamped> poses = load_csv("/home/youkely/3d_vision/recording_45/long_throw_depth.csv");
		return 0;
	}
}